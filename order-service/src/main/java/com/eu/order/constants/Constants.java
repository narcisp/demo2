package com.eu.order.constants;

public class Constants {

	public static final String TEXT = "^[a-zA-Z0-9 ]*$";
	
	public static final Integer SHOPPING_CART_STATUS = 0;
	public static final Integer ORDER_STATUS = 1;
	public static final String NO_ORDER_FOUND = "No order found";
	public static final String CHECKOUT_SUCCESS = "Checkout succesfully";

	private Constants(){}
	
}
