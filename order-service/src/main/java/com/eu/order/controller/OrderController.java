package com.eu.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eu.order.dto.OrderRequest;
import com.eu.order.dto.ResponseBody;
import com.eu.order.service.OrderService;

@RestController
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("/orders/active")
    public ResponseEntity<ResponseBody> getOrdersActive(@RequestParam Integer page, @RequestParam Integer limit) {
    	return orderService.getOrdersActive(page, limit);
    }
    
    @GetMapping("/orders/past")
    public ResponseEntity<ResponseBody> getOrdersPast(@RequestParam Integer page, @RequestParam Integer limit) {
    	return orderService.getOrdersPast(page, limit);
    }
    
    @PostMapping("/checkout")
    public ResponseEntity<ResponseBody> checkout(@RequestBody OrderRequest orderRequest) {
		return orderService.checkout(orderRequest);
    }
    
    @GetMapping("/getShoppingCart/{userId}")
    public ResponseEntity<ResponseBody> getShoppingCart(@PathVariable Long userId) {
		return orderService.getShoppingCart(userId);
    }
    
}
