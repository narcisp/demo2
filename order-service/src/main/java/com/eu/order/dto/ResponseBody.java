package com.eu.order.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ResponseBody {
	
	private Long totalRecords;
	private String errorMessage;
	private List<OrderDto> orderList;
	private OrderDto order;
	private List<ProductDto> productDTOS;
	private List<OrderProductDto> productOrderList;
	private String responseMessage;

	public Long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public List<OrderDto> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<OrderDto> orderList) {
		this.orderList = orderList;
	}
	public OrderDto getOrder() {
		return order;
	}
	public void setOrder(OrderDto order) {
		this.order = order;
	}
	public List<ProductDto> getProductDTOS() {
		return productDTOS;
	}
	public void setProductDTOS(List<ProductDto> productDTOS) {
		this.productDTOS = productDTOS;
	}
	public List<OrderProductDto> getProductOrderList() {
		return productOrderList;
	}
	public void setProductOrderList(List<OrderProductDto> productOrderList) {
		this.productOrderList = productOrderList;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

}
