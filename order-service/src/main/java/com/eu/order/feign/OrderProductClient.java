package com.eu.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.eu.order.dto.ResponseBody;

@FeignClient(name = "http://PRODUCT-ORDER-SERVICE/product-order")
public interface OrderProductClient {
	
	@GetMapping("/getProductsByOrderId/{orderId}")
	public ResponseEntity<ResponseBody> getProductsByOrderId(@PathVariable Long orderId); 

}
