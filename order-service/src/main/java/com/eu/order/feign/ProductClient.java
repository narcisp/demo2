package com.eu.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.eu.order.dto.ResponseBody;

@FeignClient(name = "http://PRODUCT-SERVICE/product")
public interface ProductClient {
	
	@GetMapping("/products")
	public ResponseEntity<ResponseBody> getProducts();

}
