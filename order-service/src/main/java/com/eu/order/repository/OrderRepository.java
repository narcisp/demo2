package com.eu.order.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.eu.order.entity.UserOrder;

public interface OrderRepository extends JpaRepository<UserOrder, Long> {
	
	Page<UserOrder> findByStatusAndOrderDateBefore(Integer status, LocalDate orderDate, Pageable pageable);
	Page<UserOrder> findByStatusAndOrderDateAfter(Integer status, LocalDate orderDate, Pageable pageable);
	
	Long countByStatusAndOrderDateBefore(Integer status, LocalDate orderDate);
	Long countByStatusAndOrderDateAfter(Integer status, LocalDate orderDate);

	Optional<UserOrder> findByStatusAndUserId(Integer status, Long userId);
	Optional<UserOrder> findByStatusAndId(Integer status, Long id);

}
