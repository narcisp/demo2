package com.eu.order.service;

import org.springframework.http.ResponseEntity;

import com.eu.order.dto.OrderRequest;
import com.eu.order.dto.ResponseBody;

public interface OrderService {

	ResponseEntity<ResponseBody> getOrdersActive(Integer page, Integer limit);

	ResponseEntity<ResponseBody> getOrdersPast(Integer page, Integer limit);

	ResponseEntity<ResponseBody> getShoppingCart(Long userId);

	ResponseEntity<ResponseBody> checkout(OrderRequest orderRequest);
    
}
