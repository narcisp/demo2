package com.eu.order.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.eu.order.constants.Constants;
import com.eu.order.dto.OrderDto;
import com.eu.order.dto.OrderProductDto;
import com.eu.order.dto.OrderRequest;
import com.eu.order.dto.ProductDto;
import com.eu.order.dto.ResponseBody;
import com.eu.order.entity.UserOrder;
import com.eu.order.feign.OrderProductClient;
import com.eu.order.feign.ProductClient;
import com.eu.order.repository.OrderRepository;
import com.eu.order.service.OrderService;
import com.eu.order.utils.Utils;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	ProductClient productClient;
	
	@Autowired
	OrderProductClient orderProductClient;
	
	@Override
	public ResponseEntity<ResponseBody> getOrdersActive(Integer page, Integer limit) {
		
		Pageable pageable = PageRequest.of(page, limit);
		
		List<UserOrder> listOfOrders = orderRepository.findByStatusAndOrderDateAfter(Constants.ORDER_STATUS, LocalDate.now().minusDays(1), pageable).getContent();
		
		Long totalRecords = 0l;
		
		totalRecords = orderRepository.countByStatusAndOrderDateAfter(Constants.ORDER_STATUS, LocalDate.now().minusDays(1));
		
		return makeProductResponseBody(listOfOrders, totalRecords);
		
	}

	@Override
	public ResponseEntity<ResponseBody> getOrdersPast(Integer page, Integer limit) {
		
		Pageable pageable = PageRequest.of(page, limit);
		List<UserOrder> listOfOrders = orderRepository.findByStatusAndOrderDateBefore(Constants.ORDER_STATUS, LocalDate.now(), pageable).getContent();
		
		Long totalRecords = 0l;
		
		totalRecords = orderRepository.countByStatusAndOrderDateBefore(Constants.ORDER_STATUS, LocalDate.now());
		
		return makeProductResponseBody(listOfOrders, totalRecords);
		
	}

	private ResponseEntity<ResponseBody> makeProductResponseBody(List<UserOrder> listOfOrders, Long totalRecords) {
		ResponseBody responseBody = new ResponseBody();
		List<OrderDto> orderList = new ArrayList<>();
		responseBody.setOrderList(orderList);
		responseBody.setTotalRecords(totalRecords);
		
		listOfOrders.forEach(element -> {
			orderList.add(Utils.mapOrderToDto(element));
		});
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseBody> getShoppingCart(Long userId) {
		
		Optional<UserOrder> shoppingCartOptional = orderRepository.findByStatusAndUserId(Constants.SHOPPING_CART_STATUS, userId);
		UserOrder shoppingCart = null;
		
		if (shoppingCartOptional.isPresent()) {
			shoppingCart = shoppingCartOptional.get();
		} else {
			shoppingCart = new UserOrder();
			shoppingCart.setStatus(Constants.SHOPPING_CART_STATUS);
			shoppingCart.setUserId(userId);
			shoppingCart = orderRepository.save(shoppingCart);
		}
		
		
		List<ProductDto> productList = productClient.getProducts().getBody().getProductDTOS();
		ResponseEntity<ResponseBody> orderProductClientResponse = orderProductClient.getProductsByOrderId(shoppingCart.getId());
		
		List<OrderProductDto> productInShoppingCart = orderProductClientResponse.getBody().getProductOrderList();

		productList.forEach(elem -> elem.setQuantityOrdered(0));
		
		if (productInShoppingCart != null) {
			productInShoppingCart.forEach(elem -> {
				ProductDto aux = new ProductDto();
				aux.setId(elem.getProductId());
				if (productList.contains(aux)) {
					productList.get(productList.indexOf(aux)).setQuantityOrdered(elem.getQuantity());
				}
			});
		}
		
		ResponseBody responseBody = new ResponseBody();
		responseBody.setOrder(Utils.mapOrderToDto(shoppingCart));
		responseBody.setProductDTOS(productList);
		
		return Utils.makeStatus(responseBody, HttpStatus.OK);
		
	}

	@Override
	public ResponseEntity<ResponseBody> checkout(OrderRequest orderRequest) {
		ResponseBody responseBody = new ResponseBody();
		Optional<UserOrder> optionalOrder = orderRepository.findByStatusAndId(Constants.SHOPPING_CART_STATUS, orderRequest.getOrderId());
		UserOrder orderDb = null;
		
		if (optionalOrder.isPresent()) {
			orderDb = optionalOrder.get();
		} else {
			responseBody.setErrorMessage(Constants.NO_ORDER_FOUND);
			return Utils.makeStatus(responseBody, HttpStatus.NOT_FOUND);
		}
		
		orderDb.setAddress(orderRequest.getAddress());
		orderDb.setPaymentMethod(orderRequest.getPaymentMethod());
		orderDb.setOrderDate(LocalDate.now());
		orderDb.setTotalPrice(orderRequest.getTotalPrice());
		orderDb.setStatus(Constants.ORDER_STATUS);
		
		orderRepository.save(orderDb);
		
		responseBody.setResponseMessage(Constants.CHECKOUT_SUCCESS);
		return Utils.makeStatus(responseBody, HttpStatus.OK);
	}
	
}
