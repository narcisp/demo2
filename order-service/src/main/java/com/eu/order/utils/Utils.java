package com.eu.order.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.eu.order.dto.OrderDto;
import com.eu.order.dto.ResponseBody;
import com.eu.order.entity.UserOrder;

public class Utils {
	
	private Utils() {
		
	}
	
	public static ResponseEntity<ResponseBody> makeStatus(ResponseBody responseBody, HttpStatus status) {
		return new ResponseEntity<>(responseBody, status);
	}
	
	public static OrderDto mapOrderToDto(UserOrder order) {
		OrderDto orderDto = new OrderDto();
		orderDto.setOrderId(order.getId());
		orderDto.setOrderDate(order.getOrderDate());
		orderDto.setTotalPrice(order.getTotalPrice());
		return orderDto;
	}

}
