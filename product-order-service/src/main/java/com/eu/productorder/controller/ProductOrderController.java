package com.eu.productorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eu.productorder.dto.ProductOrderDto;
import com.eu.productorder.dto.ResponseBody;
import com.eu.productorder.service.ProductOrderService;

@RestController
public class ProductOrderController {

    @Autowired
    ProductOrderService productOrderService;

    @PutMapping("/updateCreateOrder")
    public ResponseEntity<ResponseBody> updateCreateOrder(@RequestBody ProductOrderDto productOrderRequest) {
        return productOrderService.updateOrder(productOrderRequest);
    }
    
    @GetMapping("/getProductsByOrderId/{orderId}")
    public ResponseEntity<ResponseBody> getProductsByOrderId(@PathVariable Integer orderId) {
       return productOrderService.getProductOrderByOrderId(orderId);
    }

}
