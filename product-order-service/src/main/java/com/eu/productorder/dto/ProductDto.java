package com.eu.productorder.dto;

public class ProductDto {

    private Integer itemNumber;
    private Long productId;
    private String name;
    private String description;
    private String thumbnailImgURL;
    private String imgURL;
    private Integer price;
    private Integer basicQuantity;
    private Integer orderQuantity;

    public Integer getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(Integer itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailImgURL() {
        return thumbnailImgURL;
    }

    public void setThumbnailImgURL(String thumbnailImgURL) {
        this.thumbnailImgURL = thumbnailImgURL;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getBasicQuantity() {
        return basicQuantity;
    }

    public void setBasicQuantity(Integer basicQuantity) {
        this.basicQuantity = basicQuantity;
    }

    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }
}
