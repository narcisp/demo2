package com.eu.productorder.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

@JsonInclude(Include.NON_NULL)
public class ResponseBody {

	public ResponseBody(String message){
		this.responseMessage = message;
	}
	private String responseMessage;
	private List<ProductOrderDto> productOrderList;

	public List<ProductOrderDto> getProductOrderList() {
		return productOrderList;
	}

	public void setProductOrderList(List<ProductOrderDto> productOrderList) {
		this.productOrderList = productOrderList;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

}

