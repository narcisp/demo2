package com.eu.productorder.repository;

import com.eu.productorder.entity.ProductOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductOrderRepository extends JpaRepository<ProductOrder, Long> {

    Optional<ProductOrder> findByOrderIdAndProductId(Long orderId, Long productId);

    List<Optional<ProductOrder>> findAllByOrderId(Long orderId);
}
