package com.eu.productorder.service;

import com.eu.productorder.dto.ProductOrderDto;
import com.eu.productorder.dto.ResponseBody;
import org.springframework.http.ResponseEntity;

public interface ProductOrderService {

    ResponseEntity<ResponseBody> updateOrder(ProductOrderDto productDto);

    ResponseEntity<ResponseBody> getProductOrderByOrderId(Integer orderId);
}
