package com.eu.productorder.service.impl;

import com.eu.productorder.dto.ProductOrderDto;
import com.eu.productorder.dto.ResponseBody;
import com.eu.productorder.entity.ProductOrder;
import com.eu.productorder.repository.ProductOrderRepository;
import com.eu.productorder.service.ProductOrderService;
import com.eu.productorder.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductOrderServiceImpl implements ProductOrderService {

    @Autowired
    ProductOrderRepository productOrderRepository;

    @Override
    public ResponseEntity<ResponseBody> updateOrder(ProductOrderDto productOrderRequest) {


        Optional<ProductOrder> productOrder;
        productOrder = productOrderRepository.findByOrderIdAndProductId(productOrderRequest.getOrderId(), productOrderRequest.getProductId());
        if (!productOrder.isPresent()) {
            ProductOrder newProductOrder = new ProductOrder();
            newProductOrder.setOrderId(productOrderRequest.getOrderId());
            newProductOrder.setProductId(productOrderRequest.getProductId());
            newProductOrder.setQuantity(productOrderRequest.getQuantity());
            productOrderRepository.save(newProductOrder);
            return Utils.makeStatus(new ResponseBody("Creation successful"), HttpStatus.OK);
        }
        productOrder.get().setQuantity(productOrderRequest.getQuantity());
        productOrderRepository.save(productOrder.get());

        return Utils.makeStatus(new ResponseBody("Update successful"), HttpStatus.OK);


//
//        if (isNewOrder(productOrderList, productOrderRequest)) {
//            return createNewOrder(productOrderRequest, response, productList, productOrderList);
//        } else {
//            return updateExistingOrder(productOrderRequest, response, productList, productOrderList);
//        }




//        if (isNewOrder(productOrderList, productOrderRequest)) {
//            return createNewOrder(productOrderRequest, response, productList, productOrderList);
//        } else {
//            return updateExistingOrder(productOrderRequest, response, productList, productOrderList);
//        }
    }

    @Override
    public ResponseEntity<ResponseBody> getProductOrderByOrderId(Integer orderId) {

        List<Optional<ProductOrder>> productOrderDbList = productOrderRepository.findAllByOrderId(Long.valueOf(orderId));
        if (productOrderDbList.isEmpty()){
            return Utils.makeStatus(new ResponseBody("No products with order id " + orderId), HttpStatus.OK);
        }
        ResponseBody responseBody = new ResponseBody("Success");
        List<ProductOrderDto> productOrderList = new ArrayList<>();
        productOrderDbList.forEach(element -> {
            ProductOrderDto productOrderDto = new ProductOrderDto();
            productOrderDto.setProductId(element.get().getProductId());
            productOrderDto.setQuantity(element.get().getQuantity());
            productOrderDto.setOrderId(element.get().getOrderId());
            productOrderList.add(productOrderDto);

        });
        responseBody.setProductOrderList(productOrderList);
        return Utils.makeStatus(responseBody, HttpStatus.OK);

    }
//
//    private ResponseEntity<ResponseBody> updateExistingOrder(ProductOrderDto productOrderRequest, ProductOrderDto response, List<ProductDto> productList, List<ProductOrder> productOrderList) {
//        productOrderRequest.getProductList().forEach(element -> {
//            ProductOrder productOrder = productOrderList.get(productOrderRequest.getProductList().indexOf(element));
//            populateProductOrder(productOrderRequest, element, productOrder);
//            populateProductList(productOrderRequest, productList, element, productOrder);
//        });
//
//        ResponseBody responseBody = createResponseBody(productOrderRequest, response, productList);
//
//        return Utils.makeStatus(responseBody, HttpStatus.OK);
//    }
//
//    private ResponseEntity<ResponseBody> createNewOrder(ProductOrderDto productOrderRequest, ProductOrderDto response,  List<ProductDto> productList, List<ProductOrder> productOrderList) {
//        productOrderRepository.deleteAll(productOrderList);
//
//        productOrderRequest.getProductList().forEach(element -> {
//            ProductOrder productOrder = new ProductOrder();
//            populateProductOrder(productOrderRequest, element, productOrder);
//            populateProductList(productOrderRequest, productList, element, productOrder);
//        });
//
//        ResponseBody responseBody = createResponseBody(productOrderRequest, response, productList);
//
//        return Utils.makeStatus(responseBody, HttpStatus.OK);
//    }
//
//    private void populateProductOrder(ProductOrderDto productOrderRequest, ProductDto element, ProductOrder productOrder) {
//        productOrder.setOrderId(productOrderRequest.getOrderId());
//        Product product = productRepository.findByName(element.getName());
//        if (product == null) throw new RuntimeException("Cannot find product with name" + element.getName());
//        productOrder.setProductId(product.getId());
//        productOrder.setQuantity(element.getOrderQuantity());
//    }
//
//    private void populateProductList(ProductOrderDto productOrderRequest, List<ProductDto> productList, ProductDto element, ProductOrder productOrder) {
//        productOrderRepository.save(productOrder);
//
//        ProductDto productDto = new ProductDto();
//        Product product = productRepository.getById(productOrder.getProductId());
//
//        productDto.setItemNumber(productOrderRequest.getProductList().indexOf(element));
//        productDto.setName(product.getName());
//        productDto.setDescription(product.getDescription());
//        productDto.setThumbnailImgURL(product.getThumbnailImgURL());
//        productDto.setImgURL(product.getImgURL());
//        productDto.setPrice(product.getPrice());
//        productDto.setBasicQuantity(product.getQuantity());
//        productDto.setOrderQuantity(element.getOrderQuantity());
//        productList.add(productDto);
//    }
//
//    private boolean isNewOrder(List<ProductOrder> productOrderList, ProductOrderDto productOrderRequest) {
//        return productOrderList == null || productOrderList.isEmpty() || productOrderList.size() != productOrderRequest.getProductList().size();
//    }
//
//    private ResponseBody createResponseBody(ProductOrderDto productOrderRequest, ProductOrderDto response, List<ProductDto> productList) {
//        ResponseBody responseBody = new ResponseBody();
//        response.setOrderId(productOrderRequest.getOrderId());
//        response.setProductList(productList);
//        responseBody.setProductOrder(response);
//        responseBody.setTotalRecords(1L);
//        return responseBody;
//    }
}
