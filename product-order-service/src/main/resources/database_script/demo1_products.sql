
insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (6, 'The cool, bubbly taste of Coca-Cola Classic is the perfect partner for sunny days and lunch with friends. Grab a classic Coke to brighten your day at work, at home or out on an adventure with the people who matter most.', 'https://m.media-amazon.com/images/I/71fZ5L5lXaL._SL1500_.jpg', 'Coca-Cola', 3, 12, 'https://m.media-amazon.com/images/I/71fZ5L5lXaL._SL1500_.jpg', 'oz');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (7, 'Great for parties and events Ideal for c-stores, food service and concessions Ingredients: Carbonated water, high fructose corn syrup, caramel color, sugar, phosphoric acid, caffeine, citric acid, natural flavor.', 'https://m.media-amazon.com/images/I/51MdjTE3AjL.jpg', 'Pepsi', 4, 24, 'https://m.media-amazon.com/images/I/51MdjTE3AjL.jpg', 'oz');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (8, 'Fanta Orange Soda, a perfect bold refreshment for all parties, events, & social gatherings! Perfect size for drinking with meals, on the go, or any time', 'https://m.media-amazon.com/images/I/51gBdp5E30L.jpg', 'Fanta', 3, 12, 'https://m.media-amazon.com/images/I/51gBdp5E30L.jpg', 'oz');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (9, 'Sprite is the world''s leading lemon-lime flavored soft drink. It is caffeine free, made with 100% natural flavors, and has a crisp, clean taste that really quenches your thirst.', 'https://m.media-amazon.com/images/I/61oAcl57w2L._SL1500_.jpg', 'Sprite', 5, 24, 'https://m.media-amazon.com/images/I/61oAcl57w2L._SL1500_.jpg', 'oz');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (10, 'The original, the one that started it all. .. Mtn Dew exhilarates and quenches with its one of a kind, bold taste. Enjoy its chuggable intense refreshment.', 'https://m.media-amazon.com/images/I/81dZvZUOD7L._SL1500_.jpg', 'Mountain Dew', 4, 20, 'https://m.media-amazon.com/images/I/81dZvZUOD7L._SL1500_.jpg', 'oz');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (11, 'Cheddar Cheese (Pasteurized Milk, Cheese Cultures, Salt, Enzymes), Whey, Butter (Cream, Salt, Buttermilk, Salt, Disodium Phosphate, Annatto Extract (color), Natural Flavor', 'https://m.media-amazon.com/images/I/81KaNOT3lUL._SL1500_.jpg', 'Anthony Premium Cheddar Cheese Powder', 35, 1, 'https://m.media-amazon.com/images/I/81KaNOT3lUL._SL1500_.jpg', 'lbs');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (12, 'Best ham you can get! Most processed ham brands claiming to be Country Ham are very different from our natural air-dried Country Hams as cured for generations on the family farms of Appalachia.', 'https://m.media-amazon.com/images/I/71RMpblht9L._SL1500_.jpg', 'Rays Country Ham', 55, 2, 'https://m.media-amazon.com/images/I/71RMpblht9L._SL1500_.jpg', 'lbs');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (13, 'A genuine product of San Francisco. People in California have been enjoying this salame for over 100 years. It is the very best made.Add more flavor to any meal, appetizers or snack. Enjoy!', 'https://m.media-amazon.com/images/I/61nt0qT8O0S._SL1500_.jpg', 'Molinari & Sons San Franciso Dry Salami', 22, 3, 'https://m.media-amazon.com/images/I/61nt0qT8O0S._SL1500_.jpg', 'lbs');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (14, 'The Augason Farms Dried Whole Egg Product is an inexpensive source of high quality protein. In addition to being convenient food storage, they’re useful in everyday cooking in any recipe that calls for eggs.', 'https://m.media-amazon.com/images/I/81oMSfJ2uXL._SL1500_.jpg', 'Augason Farms Dried Whole Egg Product', 15, 2, 'https://m.media-amazon.com/images/I/81oMSfJ2uXL._SL1500_.jpg', 'lbs');

insert into product (id, description, imgurl, name, price, quantity, thumbnail_imgurl, unit)
values (15, 'The most famous Italian pasta shapes and loved across Italy, penne, gets its name from its shape. The tube-shape with angled ends was inspired by the quill of an old style ink pen. BARILLA Penne pasta is made with non-GMO ingredients.', 'https://m.media-amazon.com/images/I/51dAepblpAL.jpg', 'BARILLA Blue Box Penne Pasta', 10, 16, 'https://m.media-amazon.com/images/I/51dAepblpAL.jpg', 'oz');

