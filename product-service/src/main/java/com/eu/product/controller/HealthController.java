package com.eu.product.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class HealthController {

	@GetMapping("/ping")
	public String getHealth() {
		return "PRODUCT-SERVICE : pong";
	}
	
}
