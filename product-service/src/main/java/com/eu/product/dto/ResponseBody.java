package com.eu.product.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

@JsonInclude(Include.NON_NULL)
public class ResponseBody {

	private List<ProductDTO> productDTOS;

	public List<ProductDTO> getProductDTOS() {
		return productDTOS;
	}

	public void setProductDTOS(List<ProductDTO> productDTOS) {
		this.productDTOS = productDTOS;
	}
}
