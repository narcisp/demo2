package com.eu.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eu.product.entity.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
