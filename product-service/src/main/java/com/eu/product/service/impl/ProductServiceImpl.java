package com.eu.product.service.impl;

import com.eu.product.dto.ProductDTO;
import com.eu.product.dto.ResponseBody;
import com.eu.product.entity.Product;
import com.eu.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.eu.product.service.ProductService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public ResponseEntity<ResponseBody> getProducts() {
        List<Product> productList = productRepository.findAll();
        return addProductsInResponseBody(productList);
    }

    @Override
    public ResponseEntity<ProductDTO> getProductById(long productId) {

        Product product = productRepository.findById(productId).get();
        if(productRepository.findById(productId).isPresent()) {
            return new ResponseEntity<>(productToProductDTOForPage(product), HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    }

    private ResponseEntity<ResponseBody> addProductsInResponseBody(List<Product> productList) {
        ResponseBody responseBody = new ResponseBody();
        List<ProductDTO> productDTOS = new ArrayList<>();
        for(Product product : productList) {
            productDTOS.add(productToProductDTO(product));
        }

        responseBody.setProductDTOS(productDTOS);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    private ProductDTO productToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();

        productDTO.setName(product.getName());
        productDTO.setQuantity(product.getQuantity());
        productDTO.setUnit(product.getUnit());
        productDTO.setPrice(product.getPrice());
        productDTO.setId(product.getId());
        productDTO.setThumbnailImgUrl(product.getThumbnailImgURL());
        return productDTO;
    }

    private ProductDTO productToProductDTOForPage(Product product) {
        ProductDTO productDTO = productToProductDTO(product);
        productDTO.setImgURL(product.getImgURL());
        productDTO.setThumbnailImgUrl(null);
        productDTO.setDescription(product.getDescription());
        return productDTO;
    }
}
