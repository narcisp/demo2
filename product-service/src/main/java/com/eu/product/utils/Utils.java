package com.eu.product.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.eu.product.dto.ResponseBody;

public class Utils {
	
	private Utils() {
		
	}
	
	public static ResponseEntity<ResponseBody> makeStatus(ResponseBody responseBody, HttpStatus status) {
		return new ResponseEntity<>(responseBody, status);
	}

}
